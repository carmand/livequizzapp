# LiveQuizz

LiveQuizz is a POC of a realtime multiplayer quizz game developed in Javascript.

## Built With

* [NodeJS](https://nodejs.org/) - The backend Javascript runtime used
* [Express.js](https://expressjs.com/) - The backend Javascript framework used
* [ReactJS](https://reactjs.org/) - The frontend framework used
* [Redux](https://redux.js.org/) - Used to store application state
* [Socket.io](https://socket.io/) - Used to handle real-time communication
* [MongoDB](https://www.mongodb.com/) - Used to store questions
* [Jest](https://jestjs.io/) - Used for unit tests
* [Jenkins](https://jenkins.io/) - Used for CI/CD

## Authors

* **Charles ARMAND**